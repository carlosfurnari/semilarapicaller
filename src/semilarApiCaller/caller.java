package semilarApiCaller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class caller {
	
	static List<String> BFlist;
	static List<String> WWTlist;
	static List<String> NTlist;
	static List<String> AMlist;
	static List<String> AMlistUpdated;
	
	static PrintWriter log;

	public static void main(String[] args) throws ClientProtocolException, JSONException, IOException, InterruptedException {
		// TODO Auto-generated method stub
		
		log = new PrintWriter("logC.txt", "UTF-8");
		
		BFlist = new ArrayList<String>();
		BFlist.add("true");
		BFlist.add("false");
		
		WWTlist = new ArrayList<String>();
		WWTlist.add("NONE");
		WWTlist.add("IDF");
		WWTlist.add("ENTROPY");
		
		NTlist = new ArrayList<String>();
		NTlist.add("AVERAGE");
		NTlist.add("GEOMETRIC");
		NTlist.add("MAX");
		NTlist.add("MIN");
		NTlist.add("TEXTA");
		NTlist.add("TEXTB");
		
		AMlist = new ArrayList<String>();
		AMlist.add("LDA");
		AMlist.add("LSA");
		AMlist.add("LESK");
		AMlist.add("LESKT");
		AMlist.add("LESKTN");
		AMlist.add("HSO");
		AMlist.add("JCN");
		AMlist.add("LCH");
		AMlist.add("LIN");
		AMlist.add("PATH");
		AMlist.add("RES");
		AMlist.add("WUP");
		
		AMlistUpdated = new ArrayList<String>();
		AMlistUpdated.add("JCN");
		AMlistUpdated.add("LCH");
		AMlistUpdated.add("PATH");
		AMlistUpdated.add("WUP");
		
		String word = "To prevent accidents on the rail level crossing, a security camera system is required to be installed, which must provide video evidence of all the events on the scene. Also this systems will be part of the stock of “Serbian Railsways”";
	
		logIn("Se inician las comparaciones");
		//compareBleu(word);
		//compareMeteor(word);
		//compareLexical(word);
		//compareCorley(word);
		// como este comparador dio los mismos resultados para todos los parametros, se selecciono solo uno (true-NONE),
		// creando el nuevo metodo compareCorleyUpdated(word)
		//compareCorleyUpdated(word); 
		//compareGreedy(word);
		// Para algunos parametros dio los mismos resultados en otras pruebas, por lo tanto se achica el scope descartando estos parametros,
		// se crea el nuevo metodo con estos cambios compareGreedyUpdated(word)
		compareGreedyUpdated(word);
		//compareOptimum(word);
		//compareOptimumUpdated(word);
		//compareDependency(word);
		//compareDependencyUpdated(word);
		logIn("Se teminan las comparaciones");
		log.close();
		
	}
	
	
	
	public static void compareBleu(String word) throws ClientProtocolException, JSONException, IOException, InterruptedException{
		RequestMSG req = new RequestMSG("STANFORD", "STANFORD", "STANFORD", "STANFORD", "0.3",
				"false", "true", "false", "false", "false", "false", "false", "false", word);
		
		String result = simpleCall(req);
		System.out.println("id de comparacion: " + result);
		logIn("Bleau - Iniciado");
		String result2 = getResults(result);
		System.out.println(result2);
		while (result2.equals("The request is not solved yet")){
			Thread.sleep(300000);
			result2 = getResults(result);
			System.out.println(result2);
		}
		PrintWriter writer = new PrintWriter("bleu.txt", "UTF-8");
		writer.println(result2);
		writer.close();
		logIn("Bleau - Finalizado");
		
	}
	
	public static void compareMeteor(String word) throws ClientProtocolException, JSONException, IOException, InterruptedException{
		RequestMSG req = new RequestMSG("STANFORD", "STANFORD", "STANFORD", "STANFORD", "0.3",
				"false", "false", "true", "false", "false", "false", "false", "false", word);
		
		String result = simpleCall(req);
		
		logIn("Meteor - Iniciado");
		String result2 = getResults(result);
		System.out.println(result2);
		while (result2.equals("The request is not solved yet")){
			Thread.sleep(300000);
			result2 = getResults(result);
			System.out.println(result2);
		}
		PrintWriter writer = new PrintWriter("meteor.txt", "UTF-8");
		writer.println(result2);
		writer.close();
		
		logIn("Meteor - Finalizado");
		
	}
	
	public static void compareLexical(String word) throws ClientProtocolException, JSONException, IOException, InterruptedException{
		
		
		RequestMSG req = new RequestMSG("STANFORD", "STANFORD", "STANFORD", "STANFORD", "0.3",
				"false", "false", "false", "true", "false", "false", "false", "false", word);
		
		req.setLexicalBF("false");
		
		String result = simpleCall(req);
		
		logIn("Lexical-True - Iniciado");
		String result2 = getResults(result);
		System.out.println(result2);
		while (result2.equals("The request is not solved yet")){
			Thread.sleep(400000);
			result2 = getResults(result);
			System.out.println(result2);
		}
		PrintWriter writer = new PrintWriter("lexicalFalse.txt", "UTF-8");
		writer.println(result2);
		writer.close();
		
		logIn("Lexical-True - Finalizado");
		logIn("Lexical-False - Iniciado");
		req.setLexicalBF("true");
		
		result = simpleCall(req);
		
		result2 = getResults(result);
		System.out.println(result2);
		while (result2.equals("The request is not solved yet")){
			Thread.sleep(400000);
			result2 = getResults(result);
			System.out.println(result2);
		}
		
		writer = new PrintWriter("lexicalTrue.txt", "UTF-8");
		writer.println(result2);
		writer.close();
		logIn("Lexical-False - Finalizado");
	}
	
	public static void compareCorley(String word) throws ClientProtocolException, JSONException, IOException, InterruptedException{
		
		RequestMSG req = new RequestMSG("STANFORD", "STANFORD", "STANFORD", "STANFORD", "0.3",
				"false", "false", "false", "false", "false", "false", "false", "true", word);
		
		String fileName;
		PrintWriter writer;
		for (int i=0; i<BFlist.size(); i++) {
			for (int j=0; j<WWTlist.size(); j++) {
				req.setCorleyBF(BFlist.get(i));
				req.setCorleyWWT(WWTlist.get(j));
				
				String result = simpleCall(req);
				
				logIn("Corley-" + BFlist.get(i) + "-" + WWTlist.get(j) + " Iniciado");
				String result2 = getResults(result);
				System.out.println(result2);
				while (result2.equals("The request is not solved yet")){
					Thread.sleep(400000);
					result2 = getResults(result);
				}
				fileName = "Corley-" + BFlist.get(i) + "-" + WWTlist.get(j) + ".txt";
				writer = new PrintWriter(fileName, "UTF-8");
				writer.println(result2);
				writer.close();
				logIn("Corley-" + BFlist.get(i) + "-" + WWTlist.get(j) + " Finalizado");
			}
		}
	}
	
	public static void compareCorleyUpdated(String word) throws ClientProtocolException, JSONException, IOException, InterruptedException{
				
		RequestMSG req = new RequestMSG("STANFORD", "STANFORD", "STANFORD", "STANFORD", "0.3",
				"false", "false", "false", "false", "false", "false", "false", "true", word);
		
		String fileName;
		PrintWriter writer;
		req.setCorleyBF("true");
		req.setCorleyWWT("NONE");
		
		String result = simpleCall(req);
		logIn("Corley - Iniciado");
		String result2 = getResults(result);
		System.out.println(result2);
		while (result2.equals("The request is not solved yet")){
			Thread.sleep(400000);
			result2 = getResults(result);
		}
		fileName = "Corley-true-NONE.txt";
		writer = new PrintWriter(fileName, "UTF-8");
		writer.println(result2);
		writer.close();
		logIn("Corley - Finalizado");
	}
	
	public static void compareGreedy(String word) throws ClientProtocolException, JSONException, IOException, InterruptedException{
		
		RequestMSG req = new RequestMSG("STANFORD", "STANFORD", "STANFORD", "STANFORD", "0.3",
				"false", "false", "false", "false", "true", "false", "false", "false", word);
		
		String fileName, result, result2;
		PrintWriter writer;
		for (int i=0; i<BFlist.size(); i++) {
			for (int j=0; j<AMlist.size(); j++) {
				req.setGreedyBF(BFlist.get(i));
				req.setGreedyAM(AMlist.get(j));
				
				result = simpleCall(req);
				logIn("Greedy-" + BFlist.get(i) + "-" + AMlist.get(j) + " Iniciado");
				result2 = getResults(result);

				while (result2.equals("The request is not solved yet")){
					Thread.sleep(400000);
					result2 = getResults(result);
				}
				fileName = "Greedy-" + BFlist.get(i) + "-" + AMlist.get(j) + ".txt";
				writer = new PrintWriter(fileName, "UTF-8");
				writer.println(result2);
				writer.close();
				
				logIn("Greedy-" + BFlist.get(i) + "-" + AMlist.get(j) + " Finalizado");
			}
		}
	}
	
	public static void compareGreedyUpdated(String word) throws ClientProtocolException, JSONException, IOException, InterruptedException{
		
		RequestMSG req = new RequestMSG("STANFORD", "STANFORD", "STANFORD", "STANFORD", "0.3",
				"false", "false", "false", "false", "true", "false", "false", "false", word);
		
		String fileName, result, result2;
		PrintWriter writer;
		for (int i=0; i<BFlist.size(); i++) {
			for (int j=0; j<AMlistUpdated.size(); j++) {
				req.setGreedyBF(BFlist.get(i));
				req.setGreedyAM(AMlistUpdated.get(j));
				
				result = simpleCall(req);
				logIn("Greedy-" + BFlist.get(i) + "-" + AMlistUpdated.get(j) + " Iniciado");
				result2 = getResults(result);

				while (result2.equals("The request is not solved yet")){
					Thread.sleep(300000);
					result2 = getResults(result);
				}
				fileName = "Greedy-" + BFlist.get(i) + "-" + AMlistUpdated.get(j) + ".txt";
				writer = new PrintWriter(fileName, "UTF-8");
				writer.println(result2);
				writer.close();
				
				logIn("Greedy-" + BFlist.get(i) + "-" + AMlistUpdated.get(j) + " Finalizado");
			}
		}
	}
	
	public static void compareOptimum(String word) throws ClientProtocolException, JSONException, IOException, InterruptedException{
		
		RequestMSG req = new RequestMSG("STANFORD", "STANFORD", "STANFORD", "STANFORD", "0.3",
				"false", "false", "false", "false", "false", "false", "true", "false", word);
		
		String fileName, result, result2;
		PrintWriter writer;
		for (int i=0; i<BFlist.size(); i++) {
			for (int j=0; j<AMlist.size(); j++) {
				for (int z=0; z<WWTlist.size(); z++) {
					for (int x=0; x<NTlist.size(); x++) {
						
						req.setOptimumBF(BFlist.get(i));
						req.setOptimumAM(AMlist.get(j));
						req.setOptimumWWT(WWTlist.get(z));
						req.setOptimumNT(NTlist.get(x));
						
						result = simpleCall(req);
						logIn("Optimum-" + BFlist.get(i) + "-" + AMlist.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) + " Iniciado");
						result2 = getResults(result);

						while (result2.equals("The request is not solved yet")){
							Thread.sleep(400000);
							result2 = getResults(result);
						}
						fileName = "Optimum-" + BFlist.get(i) + "-" + AMlist.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) + ".txt";
						writer = new PrintWriter(fileName, "UTF-8");
						writer.println(result2);
						writer.close();
						logIn("Optimum-" + BFlist.get(i) + "-" + AMlist.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) + " Finalizado");
					}
				}
			}
		}
	}
	
	public static void compareOptimumUpdated(String word) throws ClientProtocolException, JSONException, IOException, InterruptedException{
		
		RequestMSG req = new RequestMSG("STANFORD", "STANFORD", "STANFORD", "STANFORD", "0.3",
				"false", "false", "false", "false", "false", "false", "true", "false", word);
		
		String fileName, result, result2;
		PrintWriter writer;
		for (int i=0; i<BFlist.size(); i++) {
			for (int j=0; j<AMlistUpdated.size(); j++) {
				for (int z=0; z<WWTlist.size(); z++) {
					for (int x=0; x<NTlist.size(); x++) {
						
						req.setOptimumBF(BFlist.get(i));
						req.setOptimumAM(AMlistUpdated.get(j));
						req.setOptimumWWT(WWTlist.get(z));
						req.setOptimumNT(NTlist.get(x));
						
						result = simpleCall(req);
						logIn("Optimum-" + BFlist.get(i) + "-" + AMlistUpdated.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) + " Iniciado");
						result2 = getResults(result);

						while (result2.equals("The request is not solved yet")){
							Thread.sleep(400000);
							result2 = getResults(result);
						}
						fileName = "Optimum-" + BFlist.get(i) + "-" + AMlistUpdated.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) + ".txt";
						writer = new PrintWriter(fileName, "UTF-8");
						writer.println(result2);
						writer.close();
						logIn("Optimum-" + BFlist.get(i) + "-" + AMlistUpdated.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) + " Finalizado");
					}
				}
			}
		}
	}
	
	public static void compareDependency(String word) throws ClientProtocolException, JSONException, IOException, InterruptedException{
		
		RequestMSG req = new RequestMSG("STANFORD", "STANFORD", "STANFORD", "STANFORD", "0.3",
				"false", "false", "false", "false", "false", "true", "false", "false", word);
		
		String fileName, result, result2;
		PrintWriter writer;
		for (int i=0; i<BFlist.size(); i++) {
			for (int j=0; j<AMlist.size(); j++) {
				for (int z=0; z<WWTlist.size(); z++) {
					for (int x=0; x<NTlist.size(); x++) {
						
						req.setDependencyBF(BFlist.get(i));
						req.setDependencyAM(AMlist.get(j));
						req.setDependencyWWT(WWTlist.get(z));
						req.setDependencyNT(NTlist.get(x));
						
						result = simpleCall(req);
						logIn("Dependency-" + BFlist.get(i) + "-" + AMlist.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) + " Iniciado");
						result2 = getResults(result);

						while (result2.equals("The request is not solved yet")){
							Thread.sleep(400000);
							result2 = getResults(result);
						}
						fileName = "Dependency-" + BFlist.get(i) + "-" + AMlist.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) +".txt";
						writer = new PrintWriter(fileName, "UTF-8");
						writer.println(result2);
						writer.close();
						
						logIn("Dependency-" + BFlist.get(i) + "-" + AMlist.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) + " Finalizado");
					}
				}
			}
		}
	}
	
	public static void compareDependencyUpdated(String word) throws ClientProtocolException, JSONException, IOException, InterruptedException{
		
		RequestMSG req = new RequestMSG("STANFORD", "STANFORD", "STANFORD", "STANFORD", "0.3",
				"false", "false", "false", "false", "false", "true", "false", "false", word);
		
		String fileName, result, result2;
		PrintWriter writer;
		for (int i=0; i<BFlist.size(); i++) {
			for (int j=0; j<AMlistUpdated.size(); j++) {
				for (int z=0; z<WWTlist.size(); z++) {
					for (int x=0; x<NTlist.size(); x++) {
						
						req.setDependencyBF(BFlist.get(i));
						req.setDependencyAM(AMlistUpdated.get(j));
						req.setDependencyWWT(WWTlist.get(z));
						req.setDependencyNT(NTlist.get(x));
						
						result = simpleCall(req);
						logIn("Dependency-" + BFlist.get(i) + "-" + AMlistUpdated.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) + " Iniciado");
						result2 = getResults(result);

						while (result2.equals("The request is not solved yet")){
							Thread.sleep(400000);
							result2 = getResults(result);
						}
						fileName = "Dependency-" + BFlist.get(i) + "-" + AMlistUpdated.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) +".txt";
						writer = new PrintWriter(fileName, "UTF-8");
						writer.println(result2);
						writer.close();
						
						logIn("Dependency-" + BFlist.get(i) + "-" + AMlistUpdated.get(j) + "-" + WWTlist.get(z) + "-" + NTlist.get(x) + " Finalizado");
					}
				}
			}
		}
	}
	
	public static String simpleCall(RequestMSG input) throws JSONException, ClientProtocolException, IOException {
		String URI3 = "http://localhost:8082/semilar/compareToDB";
		JSONObject json = new JSONObject();
		json.put("tokenizer", input.getTokenizer());
		json.put("tagger", input.getTagger()); 
		json.put("stemmer", input.getStemmer()); 
		json.put("parser", input.getParser());
		json.put("threshold", input.getThreshold());
		json.put("greedy", input.getGreedy());
		json.put("greedyAM", input.getGreedyAM());
		json.put("greedyBF", input.getGreedyBF());		
		json.put("optimum", input.getOptimum());
		json.put("optimumAM", input.getOptimumAM());
		json.put("optimumBF", input.getOptimumBF());
		json.put("optimumWWT", input.getOptimumWWT());
		json.put("optimumNT", input.getOptimumNT());
		json.put("depency", input.getDepency());
		json.put("dependencyAM", input.getDependencyAM());
		json.put("dependencyBF", input.getDependencyBF());
		json.put("dependencyWWT", input.getDependencyWWT());
		json.put("dependencyNT", input.getDependencyNT());
		json.put("corley", input.getCorley());
		json.put("corleyBF", input.getCorleyBF());
		json.put("corleyWWT", input.getCorleyWWT());
		json.put("lexical", input.getLexical());
		json.put("lexicalBF", input.getLexicalBF());
		json.put("meteor", input.getMeteor());
		json.put("bleu", input.getBleu());
		json.put("lsa", input.getLsa());
		json.put("word1", input.getWord1());
		
		//txtResponse.setText(json.toString());
		//test.setText(json.toString());
		System.out.println("el json a enviar es: ");
		System.out.println(json.toString());
		
		String results = "";
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpPost request = new HttpPost(URI3);
			StringEntity params = new StringEntity(json.toString());
		    request.addHeader("content-type", "application/json");
		    request.addHeader("Accept","application/json");
		    request.setEntity(params);
		    HttpResponse response = httpClient.execute(request);
		    String resp = response.toString();
		    System.out.println("response desde el semilar controller");
		    System.out.println(resp);
		    String json_string = EntityUtils.toString(response.getEntity());
		    results = json_string;
		} finally {
			httpClient.close();
		}
		return results;
	}
	
	public static String getResults(String num) throws ClientProtocolException, IOException{
		String URI4 = "http://localhost:8082/semilar/database/getResponse/" + num;
		System.out.println(URI4);
		String results = "";
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpGet request = new HttpGet(URI4);
			//StringEntity params = new StringEntity(json.toString());
		    request.addHeader("content-type", "application/json");
		    request.addHeader("Accept","application/json");
		    //request.setEntity(params);
		    HttpResponse response = httpClient.execute(request);
		    String resp = response.toString();
		    System.out.println("response desde el semilar ante el get results");
		    System.out.println(resp);
		    String json_string = EntityUtils.toString(response.getEntity());
		    results = json_string;
		} finally {
			httpClient.close();
		}
		return results;
	}
	
	public static void logIn(String input) throws FileNotFoundException, UnsupportedEncodingException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		log.println(dateFormat.format(date) + ": " + input);
		
	}
}
