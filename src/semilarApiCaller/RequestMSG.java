package semilarApiCaller;

public class RequestMSG {
	
	private String tokenizer;
	private String tagger;
	private String stemmer;
	private String parser;
	
	private String threshold;
	
	private String bleu;
	private String meteor;
	private String lsa;
	private String greedy;
	private String lexical;
	private String corley;
	private String optimum;
	private String depency;
	
	private String lexicalBF = "";
	private String corleyBF = "";
	private String corleyWWT = "";
	private String greedyAM = "";
	private String greedyBF = "";
	private String optimumAM = "";
	private String optimumBF = "";
	private String optimumWWT = "";
	private String optimumNT = "";
	private String dependencyAM = "";
	private String dependencyBF = "";
	private String dependencyWWT = "";
	private String dependencyNT = "";
	
	private String word1;
	
	public RequestMSG(String tokenizer, String tagger, String stemmer, String parser, String threshold, 
			String lsa, String bleu, String meteor,	String lexical, String greedy, String depency, String optimum, String corley, 
			String word){
		this.setTokenizer(tokenizer);
		this.setTagger(tagger);
		this.setStemmer(stemmer);
		this.setParser(parser);
		
		this.setThreshold(threshold);
		
		this.setGreedy(greedy);
		this.setBleu(bleu);
		this.setLexical(lexical);
		this.setMeteor(meteor);
		this.setLsa(lsa);
		this.setCorley(corley);
		this.setOptimum(optimum);
		this.setDepency(depency);
		
		this.setWord1(word);
	}

	public String getTokenizer() {
		return tokenizer;
	}

	public void setTokenizer(String tokenizer) {
		this.tokenizer = tokenizer;
	}

	public String getTagger() {
		return tagger;
	}

	public void setTagger(String tagger) {
		this.tagger = tagger;
	}

	public String getStemmer() {
		return stemmer;
	}

	public void setStemmer(String stemmer) {
		this.stemmer = stemmer;
	}

	public String getParser() {
		return parser;
	}

	public void setParser(String parser) {
		this.parser = parser;
	}

	public String getThreshold() {
		return threshold;
	}

	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}

	public String getBleu() {
		return bleu;
	}

	public void setBleu(String bleu) {
		this.bleu = bleu;
	}

	public String getMeteor() {
		return meteor;
	}

	public void setMeteor(String meteor) {
		this.meteor = meteor;
	}

	public String getLsa() {
		return lsa;
	}

	public void setLsa(String lsa) {
		this.lsa = lsa;
	}

	public String getGreedy() {
		return greedy;
	}

	public void setGreedy(String greedy) {
		this.greedy = greedy;
	}

	public String getLexical() {
		return lexical;
	}

	public void setLexical(String lexical) {
		this.lexical = lexical;
	}

	public String getCorley() {
		return corley;
	}

	public void setCorley(String corley) {
		this.corley = corley;
	}

	public String getOptimum() {
		return optimum;
	}

	public void setOptimum(String optimum) {
		this.optimum = optimum;
	}

	public String getDepency() {
		return depency;
	}

	public void setDepency(String depency) {
		this.depency = depency;
	}

	public String getLexicalBF() {
		return lexicalBF;
	}

	public void setLexicalBF(String lexicalBF) {
		this.lexicalBF = lexicalBF;
	}

	public String getCorleyBF() {
		return corleyBF;
	}

	public void setCorleyBF(String corleyBF) {
		this.corleyBF = corleyBF;
	}

	public String getCorleyWWT() {
		return corleyWWT;
	}

	public void setCorleyWWT(String corleyWWT) {
		this.corleyWWT = corleyWWT;
	}

	public String getGreedyBF() {
		return greedyBF;
	}

	public void setGreedyBF(String greedyBF) {
		this.greedyBF = greedyBF;
	}

	public String getGreedyAM() {
		return greedyAM;
	}

	public void setGreedyAM(String greedyAM) {
		this.greedyAM = greedyAM;
	}

	public String getOptimumAM() {
		return optimumAM;
	}

	public void setOptimumAM(String optimumAM) {
		this.optimumAM = optimumAM;
	}

	public String getOptimumBF() {
		return optimumBF;
	}

	public void setOptimumBF(String optimumBF) {
		this.optimumBF = optimumBF;
	}

	public String getOptimumWWT() {
		return optimumWWT;
	}

	public void setOptimumWWT(String optimumWWT) {
		this.optimumWWT = optimumWWT;
	}

	public String getOptimumNT() {
		return optimumNT;
	}

	public void setOptimumNT(String optimumNT) {
		this.optimumNT = optimumNT;
	}

	public String getDependencyAM() {
		return dependencyAM;
	}

	public void setDependencyAM(String dependencyAM) {
		this.dependencyAM = dependencyAM;
	}

	public String getDependencyBF() {
		return dependencyBF;
	}

	public void setDependencyBF(String dependencyBF) {
		this.dependencyBF = dependencyBF;
	}

	public String getDependencyWWT() {
		return dependencyWWT;
	}

	public void setDependencyWWT(String dependencyWWT) {
		this.dependencyWWT = dependencyWWT;
	}

	public String getDependencyNT() {
		return dependencyNT;
	}

	public void setDependencyNT(String dependencyNT) {
		this.dependencyNT = dependencyNT;
	}

	public String getWord1() {
		return word1;
	}

	public void setWord1(String word1) {
		this.word1 = word1;
	}
}
